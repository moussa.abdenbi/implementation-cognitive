import sys
import time
import random
from collections import deque
from itertools import combinations
from copy import deepcopy
from utilities_functions import *

INFINI = - sys.maxint - 1

def rank_vertices(D, I):
    V_I = deepcopy(I.keys())
    d_I = delta(I)
    E_S = { u : INFINI for u in set(D.keys()).difference(set(V_I))}
    for u in E_S.keys():
        V_I.append(u)
        I_prim = induced_graph(D, V_I)
        if is_acyclic(I_prim):
            E_S[u] = delta(I_prim) - d_I
        V_I.pop()
    return E_S

def greedy_algorithm(D, M, i):
    I = induced_graph(D, M)
    if not is_acyclic(I) or len(M) > i:
        return INFINI
    while len(I) < i:
        E_S = rank_vertices(D, I)
        u = max(E_S, key = E_S.get)
        if E_S[u] != INFINI:
            V = I.keys()
            V.append(u)
            I = induced_graph(D, V)
        else:
            return INFINI
    return delta(I)

path = sys.argv[1]
M = sys.argv[2].split(',')
D = load_graph(path)
for i in range(len(D) - len(M)):
    start = time.time()
    d = greedy_algorithm(D, M, i + len(M))
    end = time.time()
    print(str(i + len(M)) + ',' + str(d) + ',' + str(end - start))