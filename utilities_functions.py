def load_graph(path):
    f = open(path, 'r').readlines()
    n, m = f[0].split()
    D = {str(i) : [] for i in range(int(n))}
    for i in range(1, len(f)):
        arc = f[i].split()
        D[arc[0]].append(arc[1])
    return D

def delta(I):
    I_plus = {u : 0 for u in I.keys()}
    I_moins = {u : 0 for u in I.keys()}
    p = 0
    s = 0
    for u, ajd in I.items():
        for v in ajd:
            I_plus[u] += 1
            I_moins[v] += 1
    for u in I_plus.keys():
        if I_plus[u] == 0 and I_moins[u] > 0:
            p += 1
        elif I_plus[u] > 0 and I_moins[u] == 0:
            s += 1
    return p - s

def induced_graph(D, V):
    I = {u : [] for u in V}
    for u in V:
        for v in V:
            if v in D[u]:
                I[u].append(v)
    return I

def circuit(I, v, M, P):
    M.append(v)
    P.append(v)
    for u in I[v]:
        if not u in M:
            if circuit(I, u, M, P):
                return True
        elif u in P:
            return True
    P.pop()
    return False

def is_acyclic(I):
    M = []
    P = []
    for v in I.keys():
        if not v in M:
            if circuit(I, v, M, P):
                return False
    return True

def zero_deg(X):
    for v in X.keys():
        if neg_deg(v, X) == 0:
            return v
    return None

def del_vertex(v, X):
    if v in X:
        del X[v]
    for val in X.values():
        if v in val:
            val.remove(v)

def neg_deg(v, X):
    deg = 0
    for val in X.values():
        if v in val:
            deg += 1
    return deg

def max_deg(X):
    d_max = X.keys()[0]
    for key, val in X.items():
        if len(val) > len(X[d_max]):
            d_max = key
    return d_max

def extract_strategy(I):
    return sorted(I, key = lambda u: len(I[u]), reverse=True)

def strategy_cost(S, X):
    cost = 0
    while len(S) > 0 and len(X) > 0:
        l = S[0]
        del S[0]
        del_vertex(l, X)
        cost += 1
        deg = zero_deg(X)
        while deg != None:
            del_vertex(deg, X)
            deg = zero_deg(X)

    return cost, X

def complete_cost(S, X):
    cost, X_prim = strategy_cost(S, X)
    while len(X) > 0:
        l = max_deg(X)
        del_vertex(l, X)
        cost += 1
        deg = zero_deg(X)
        while deg != None:
            del_vertex(deg, X)
            deg = zero_deg(X)
    return cost
